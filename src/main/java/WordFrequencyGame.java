import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import java.util.stream.Collectors;

public class WordFrequencyGame {

    public String getWordFrequency(String inputStr) {


        String splitWord = "\\s+";
        if (inputStr.split(splitWord).length == 1) {
            return inputStr + " 1";
        }
        try {
            List<Input> wordCountList = wordCounter(wordList(inputStr));

            return wordCountList.stream()
                    .map(w -> w.getValue() + " " + w.getWordCount())
                    .collect(Collectors.joining("\n"));
        } catch (Exception e) {
            return "Calculate Error";
        }

    }
    private Map<String, List<Input>> wordList(String inputStr){
        String splitWord = "\\s+";
        String[] inputStrArray = inputStr.split(splitWord);
        List<Input> inputList = new ArrayList<>();

        for (String inputStrElement : inputStrArray) {
            Input input = new Input(inputStrElement, 1);
            inputList.add(input);
        }
        return getListMap(inputList);
    }
    private List<Input> wordCounter(Map<String, List<Input>> wordCount) {
        return wordCount.entrySet().stream()
                .map(entry -> new Input(entry.getKey(), entry.getValue().size())).sorted((word1, word2) -> word2.getWordCount() - word1.getWordCount()).collect(Collectors.toList());
    }

    private Map<String, List<Input>> getListMap(List<Input> inputList) {
        Map<String, List<Input>> map = new HashMap<>();

        for (Input input : inputList) {
            map.computeIfAbsent(input.getValue(), k -> new ArrayList<>()).add(input);
        }

        return map;
    }


}

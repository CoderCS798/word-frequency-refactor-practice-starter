## O:

I learned about code refactoring and which way to write code is better in which situation.

## R:

I fewl benefited a lot.

## I:

I think the naming of variables is very important, I used to write code often with some insubstantial names, and when I look back later, I can't understand the meaning of the code that I wrote at the beginning, so it is very important to have a meaningful name when programming.

## D:

In the future, when I program, I'll pay attention to variable naming, use streamAPI instead of loops to avoid duplicating code, etc.